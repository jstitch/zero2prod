FROM rust:1.80.1

WORKDIR /app

RUN apt update && apt install lld clang -y

COPY . .

ENV SQLX_OFFLINE true

RUN cargo clean
RUN cargo build --release

ENTRYPOINT ["./target/release/zero2prod"]
