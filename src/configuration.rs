use secrecy::ExposeSecret;
use secrecy::Secret;

#[derive(Debug, serde::Deserialize)]
pub struct Settings {
    pub database: DatabaseSettings,
    pub application: ApplicationSettings,
}

#[derive(Debug, serde::Deserialize)]
pub struct ApplicationSettings {
    pub port: u16,
    pub host: String,
}

#[derive(Debug, serde::Deserialize)]
pub struct DatabaseSettings {
    pub username: String,
    pub password: Secret<String>,
    pub port: u16,
    pub host: String,
    pub database_name: String,
}

impl DatabaseSettings {
    pub fn db_username(&self) -> String {
        std::env::var("DB_USER")
            .unwrap_or(self.username.clone())
            .to_string()
    }
    pub fn db_password(&self) -> String {
        std::env::var("DB_PASSWORD")
            .unwrap_or(self.password.expose_secret().clone())
            .to_string()
    }
    pub fn db_host(&self) -> String {
        std::env::var("DB_HOST")
            .unwrap_or(self.host.clone())
            .to_string()
    }
    pub fn db_port(&self) -> String {
        std::env::var("DB_PORT")
            .unwrap_or(self.port.to_string())
            .to_string()
    }
    pub fn db_name(&self) -> String {
        std::env::var("DB_NAME")
            .unwrap_or(self.database_name.clone())
            .to_string()
    }
    pub fn connection_string(&self) -> Secret<String> {
        Secret::new(format!(
            "postgres://{}:{}@{}:{}/{}",
            self.db_username(),
            self.password.expose_secret(),
            self.db_host(),
            self.db_port(),
            self.db_name()
        ))
    }

    pub fn connection_string_without_db(&self) -> Secret<String> {
        Secret::new(format!(
            "postgres://{}:{}@{}:{}",
            self.db_username(),
            self.password.expose_secret(),
            self.db_host(),
            self.db_port(),
        ))
    }
}

pub fn get_configuration() -> Result<Settings, config::ConfigError> {
    // Initialise our configuration reader
    let settings = config::Config::builder()
        // Add configuration values from a file named 'configuration.yaml'
        .add_source(config::File::new(
            "configuration.yaml",
            config::FileFormat::Yaml,
        ))
        .build()?;
    // Try to convert the configuration values it read into our Settings type
    settings.try_deserialize::<Settings>()
}
